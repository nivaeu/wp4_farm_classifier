﻿//
// Copyright (c) CREA 2019-2021.
// This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
// All rights reserved
//
// Project and code is made available under the EU-PL v 1.2 license.
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FarmClassifierComponent
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            SqlHelper.ConnectionString = configuration.GetConnectionString("ClassCEEConnection");
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(swagger =>
            {
                //swagger.DescribeAllEnumsAsStrings();
                //swagger.DescribeAllParametersInCamelCase();
                swagger.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "NIVA API",
                    Description = "Farm Classifier Service", 
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact
                    {
                        Name = "CREA-PB Italy",
                        Email ="rica@crea.gov.it"
                    }, 
                    Version = "V1"
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                swagger.IncludeXmlComments(xmlPath);

            });

            services.AddSingleton(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                });
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
                .WithOrigins("*")
                .AllowAnyMethod()
                .AllowCredentials()
                .WithHeaders("*")
                );
            //
            app.UseMvc();

            //app.UseWelcomePage();
            //app.UseRuntimeInfoPage();
            //app.Run(async (context) =>
            //{
            //    //var msg = $"Farm Classifier Service is UP now at {DateTime.UtcNow.ToString()} (UTC Time -Startup.cs)! ):(";
            //    //await context.Response.WriteAsync(msg);
            //    throw new System.Exception("Throw Exception");
            //});


        }
    }
}
