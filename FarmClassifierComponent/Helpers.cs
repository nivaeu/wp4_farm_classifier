﻿//
// Copyright (c) CREA 2019-2021.
// This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
// All rights reserved
//
// Project and code is made available under the EU-PL v 1.2 license.
//
using FarmClassifierComponent.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
 
namespace FarmClassifierComponent
{
    public class SqlHelper
    {
        public static string ConnectionString;
    }

    [DebuggerStepThrough]
    public static class Extensions
    {
        public static Dictionary<string, double> ToActivityDictionary(this ClassificationRequest payLoad)
        {
            var activities = new Dictionary<string, double>();
            foreach (var act in payLoad.FarmActivities)
                activities.Add(act.ActivityCode, act.Dimension);

            return activities;
        }

        public static string IntToRoman(this string numero)
        {
            int num = Convert.ToInt32(numero);
            var result = string.Empty;
            var map = new Dictionary<string, int>
            {
                {"M", 1000 },
                {"CM", 900},
                {"D", 500},
                {"CD", 400},
                {"C", 100},
                {"XC", 90},
                {"L", 50},
                {"XL", 40},
                {"X", 10},
                {"IX", 9},
                {"V", 5},
                {"IV", 4},
                {"I", 1}
            };
            foreach (var pair in map)
            {
                result += string.Join(string.Empty, Enumerable.Repeat(pair.Key, num / pair.Value));
                num %= pair.Value;
            }
            return result;
        }
    }
}
