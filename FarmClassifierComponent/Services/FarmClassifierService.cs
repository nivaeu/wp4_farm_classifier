﻿//
// Copyright (c) CREA 2019-2021.
// This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
// All rights reserved
//
// Project and code is made available under the EU-PL v 1.2 license.
//
using FarmClassifierComponent.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FarmClassifierComponent.Services
{
    public class FarmClassifierService : baseService
    {
        private List<string> warningMessages;

        public FarmClassifierService() : base()
        {
            this._Classificazione = new ClassificationResult();
            warningMessages = new List<string>();
        }

        #region private members

        private double D01 = 0;                    //FRUMENTO TENERO
        private double D02 = 0;                    //FRUMENTO DURO
        private double D03 = 0;                    //SEGALE
        private double D04 = 0;                    //ORZO
        private double D05 = 0;                    //AVENA
        private double D06 = 0;                    //Mais
        private double D07 = 0;                    //RISO
        private double D08 = 0;                    //ALTRI CEREALI
        private double D09 = 0;                    //LEGUMI SECCHI
        private double D10 = 0;                    //PATATE
        private double D11 = 0;                    //BARBABIETOLA DA ZUCCH.
        private double D12 = 0;                    //SARCHIATE DA FORAGGIO
        private double D14A = 0;                   //ORTO IN PIENO CAMPO
        private double D14B = 0;                   //ORTO INDUSTRIALE
        private double D15 = 0;                    //ORTO IN SERRA
        private double D16 = 0;                    //FIORI IN PIENA ARIA
        private double D17 = 0;                    //FIORI IN SERRA
        private double D18A = 0;                   //Prati avvicendati (medica, sulla, trifoglio, lupinella, ecc.)
        private double D18B = 0;                   //Altre foraggere verdi (mais da foraggio, erbai monofita, ecc.)
        private double D19 = 0;                    //Sementi e piantine per seminativi (sementi da prato, ecc.)
        private double D20 = 0;                    //Altri colture per seminativi (compresi affitti sotto l’anno)
        private double D22 = 0;                    //Terreni a riposo senza uso economico
        private double D23 = 0;                    //TABACCO
        private double D24 = 0;                    //Luppolo
        private double D25 = 0;                    //Cotone
        private double D26 = 0;                    //Colza e ravizzone
        private double D27 = 0;                    //Girasole
        private double D28 = 0;                    //Soia
        private double D29 = 0;                    //Semi di lino
        private double D30 = 0;                     //Altre oleaginose erbacee
        private double D31 = 0;                     //Lino
        private double D32 = 0;                     //Canapa
        private double D33 = 0;                     //Altre colture tessili
        private double D34 = 0;                     //Piante aromatiche, medicina
        private double D35 = 0;                     //Altre colture industriali
        private double F01 = 0;                    //Prati permanenti e pascoli - esclusi i pascoli magri
        private double F02 = 0;                    //Prati permanenti e pascoli - pascoli magri
        private double G01A = 0;                     //Frutteti - di origine temperata
        private double G01B = 0;                     //Frutteti - di origine subtropicale
        private double G01C = 0;                     //Frutteti - per frutta a guscio
        private double G02 = 0;                    //AGRUMETO
        private double G03A = 0;                     //Oliveti - per olive da tavola
        private double G03B = 0;                     //Oliveti - per olive da olio
        private double G04A = 0;                     //VIGNETO (VINO D.O.C.)
        private double G04B = 0;                     //VIGNETO (VINO COMUNE)
        private double G04C = 0;                     //VIGNETO(UVA DA TAVOLA)
        private double G04D = 0;                     //Vigneti per uva passa
        private double G05 = 0;                      // Vivai (semenzai e piantonai)
        private double G06 = 0;                    //ALTRE COLT. PERMANENTI
        private double G07 = 0;                    //COLT.permanenti / SERRA
        private double I02 = 0;                    //FUNGHI(MQ)

        private double J01 = 0;                    //Equini in complesso (di tutte le età)
        private double J02 = 0;                   //Bovini maschi e femmine meno di 1 anno
        private double J02unit = 0;
        private double J03 = 0;                    //BOVINI MASCHI 1-2 ANNI
        private double J04 = 0;                    //BOVINI FEMM.  1-2 ANNI
        private double J05 = 0;                    //BOVINI MASCHI > 2 ANNI
        private double J06 = 0;                    //GIOVENCHE > 2 ANNI
        private double J07 = 0;                    //VACCHE LATTIFERE
        private double J08 = 0;                    //ALTRE VACCHE
        private double J09A = 0;                     //PECORE
        private double J09B = 0;                     //ALTRI OVINI
        private double J10A = 0;                     //CAPRE
        private double J10B = 0;                     //ALTRI CAPRINI
        private double J11 = 0;                    //LATTONZOLI
        private double J12 = 0;                    //SCROFE DA RIPRODUZIONE
        private double J13 = 0;                    //ALTRI SUINI
        private double J14 = 0;                    //POLLI DA CARNE
        private double J15 = 0;                    //GALLINE DA UOVA
        private double J16D = 0;                    //Altro pollame (Tacchini, Anatre, oche)
        private double J17 = 0;                    //CONIGLIE MADRI
        private double J18 = 0;                    //API (NUMERO DI ARNIE)


        private bool P91 = false;                  //FORAGGERE REIMPIEGATE

        private double P1 = 0;                   //Seminativi
        private double P15 = 0;                    //Cereali
        private double P151 = 0;                     //Cereali senza riso
        private double P112 = 0;                     //Riso

        private double P16 = 0;                    //Proteiche e oleaginose
        private double P17 = 0;                     //Piante sarchiate

        private double P2 = 0;                   //Ortofloricoltura intensiva
        private double P3 = 0;                   //Coltivazioni permanenti
        private double P31 = 0;                    //Viticoltura
        private double P32 = 0;                    //Frutta e agrumi

        private double P4 = 0;                   //Prati ed erbivori
        private double P40 = 0;                  // Erbivori
        private double P45 = 0;                    //Bovini da latte
        private double P46 = 0;                    //Bovini

        private double P5 = 0;                   //Granivori
        private double P51 = 0;                    //Suini
        private double P52 = 0;                    //Pollame

        private double a = 0;
        private double B = 0;
        //private double _c= 0;
        //private double D= 0;
        //private double _E= 0;

        private double _NrVacche = 0;
        private double _NrVitelli = 0;
        private double PSvitelli = 0;

        public double PS = 0;
        private double DE = 0;

       #endregion 

        #region public stuff

        private Int32 _CodiceOTE = 0;
        public Int32 CodiceOTE
        { get { return this._CodiceOTE; } }

        private string _DescrizioneOTE = string.Empty;
        public string DescrizioneOTE
        { get { return this._DescrizioneOTE; } }

        private string _DescrizioneOTEPrincipale = string.Empty;
        public string DescrizioneOTEPrincipale
        { get { return this._DescrizioneOTEPrincipale; } }

        private string _DescrizioneOTEParticolare = string.Empty;
        public string DescrizioneOTEParticolare
        { get { return this._DescrizioneOTEParticolare; } }

        private string _CodiceUDEINEA = string.Empty;
        public string CodiceUDEINEA
        { get { return this._CodiceUDEINEA; } }

        private string _ClasseDimensioneEconomicaINEA = string.Empty;
        public string ClasseDimensioneEconomicaINEA
        { get { return this._ClasseDimensioneEconomicaINEA; } }

        private string _CodiceUDEUE = string.Empty;
        public string CodiceUDEUE
        { get { return this._CodiceUDEUE; } }

        private string _ClasseDimensioneEconomicaUE = string.Empty;
        public string ClasseDimensioneEconomicaUE
        { get { return this._ClasseDimensioneEconomicaUE; } }

        private ClassificationResult _Classificazione = new ClassificationResult();
        public ClassificationResult ClassificazioneAziendale
        {
            get { return _Classificazione; }
            set { _Classificazione = value; }
        }

        #endregion Proprietà Pubbliche

        public List<string> GetWarningMessages()
        {
            return warningMessages;
        }

        /// <summary>
        /// Perform classification of farm
        /// </summary>
        /// <param name="euroRegioneCode">ID REGIOEU</param>
        /// <param name="activities">Dictionary of Activities</param>
        /// <param name="refYear"></param>
        public void Run(string euroRegioneCode, Dictionary<string, double> activities, int? refYear)
        {
            computeClassification(euroRegioneCode, activities, refYear);
        }

        private void computeClassification(string euroRegionCode, Dictionary<string, double> activities, int? refYear) //9999 to consider last year avalaible
        {
            if (refYear == null)
                refYear = 9999; //will be used last SO Series avalaible in the system

            //INPUT: int ID_RedioneUE, Array [string ID_ATTIVITA, double Dimensione]

            /* 1) per ogni ID_Attivita leggere in ATTIVITA_REGIONEPS il PS
             * e moltiplicare la Dimensione per PS (PSDim)
             *  
             * 2) quindi carico le variabili in base alle Attività e PSDim
             * 
             * 3) Attribuisci OTE in base al calcolo del PS_AZIENDA)
             * 4) Attribuisci UDE in base al calcolo del PS_AZIENDA)
             */

            Dictionary<string, double> _activ = this.Calcola_PS_Attivita(euroRegionCode, refYear.Value, activities);
            this.confermentActivitiesAccumulator(_activ);
            this.confermentOTE();
            this.confermentUDE();
            this.updateLabels();
        }

        private void updateLabels()
        {
            DataTable tb = runSimpleCommand($"SELECT DESCRIZ_OTE_GENERALE, DESCRIZ_OTE_Principale, DESCRIZ_OTE_Particolari FROM FarmType WHERE ID_OTE={this._CodiceOTE}");
            if (tb.Rows.Count > 0)
            {
                this._DescrizioneOTE = tb.Rows[0]["DESCRIZ_OTE_GENERALE"].ToString();
                this._DescrizioneOTEPrincipale = tb.Rows[0]["DESCRIZ_OTE_Principale"].ToString();
                this._DescrizioneOTEParticolare = tb.Rows[0]["DESCRIZ_OTE_Particolari"].ToString();
            }
            
            //string descrUDE = runScalarCommand($"SELECT DESCRIZ FROM EconomicFarmSize WHERE ID_UDE_UE={this._CodiceUDEUE}").ToString();
            //this._ClasseDimensioneEconomicaUE = descrUDE;
            this._Classificazione.TypeFarmCodeOTE = this._CodiceOTE;
            this._Classificazione.PrincipalTypeOfFarming = this._DescrizioneOTEPrincipale.Trim();
            this._Classificazione.SubdivisionParticularTypeFarming = this._DescrizioneOTEParticolare.Trim();
            this._Classificazione.FarmEconomicSizeCode = this._CodiceUDEUE;
            this._Classificazione.FarmEconomicSizeClass = this._ClasseDimensioneEconomicaUE;
            this._Classificazione.StandardOutput = Math.Round(this.PS, 2);
        }

        

        private Dictionary<string, double> Calcola_PS_Attivita(string euroRegionCode, int year, Dictionary<string, double> Attivita)
        {
            var _SOSeriesService = new SOSeriesService();
            this._Classificazione.ReferenceSOYear = _SOSeriesService.GetYearSOSeries(year);

            string sql = $"SELECT Value FROM Activities A INNER JOIN ActivityDataStandardOutput ADet ON ADet.ActivityID = A.ActivityID WHERE ADet.EuroRegionCode = '{euroRegionCode}' and ADet.refyear = {this._Classificazione.ReferenceSOYear} ";

            Dictionary<string, double> AttivitaPS = new Dictionary<string, double>();
            foreach (var att in Attivita)
            {
                object o = runScalarCommand($"{sql} and A.EuroActivityCode = '{att.Key}'");
                double ps = 0;
                try
                {
                    ps = att.Value * this.NullToZero(o);
                }
                catch
                {
                    warningMessages.Add($"SO Value not properly set for EuroActivityCode '{att.Key}'! This activity was not considered! Check 'ActivityDataStandardOutput' table");
                }
                AttivitaPS.Add(att.Key, ps);
            }
            return AttivitaPS;
        }

        private void confermentActivitiesAccumulator(Dictionary<string, double> AttivitaPS)
        {
            foreach (var att in AttivitaPS)
            {
                #region with old code

                //var oldCodeEU = att.Key;
                ////if (false == oldClassificationMethod)
                //    oldCodeEU = convertToOLDCode(att.Key);

                //switch (oldCodeEU)
                //{
                //    case "P91":                 //FORAGGERE REIMPIEGATE
                //        P91 = true;
                //        break;

                //    case "D01":                //FRUMENTO TENERO
                //        D01 = att.Value;
                //        P1 = P1 + att.Value;    //Seminitavi + f.duro
                //        P15 = P15 + att.Value;  //Cereali + f.duro
                //        P151 = P151 + att.Value;    //Cereali senza riso + f.duro
                //        break;

                //    case "D02":               //FRUMENTO DURO
                //        D02 = att.Value;
                //        P1 = P1 + att.Value;    //Seminitavi + f.tenero
                //        P15 = P15 + att.Value;  //Cereali + f.tenero
                //        P151 = P151 + att.Value;    //Cereali senza riso + f.tenero
                //        break;

                //    case "D03":                //SEGALE
                //        D03 = att.Value;
                //        P1 = P1 + att.Value;
                //        P15 = P15 + att.Value;
                //        P151 = P151 + att.Value;
                //        break;

                //    case "D04":                //ORZO
                //        D04 = att.Value;
                //        P1 = P1 + att.Value;
                //        P15 = P15 + att.Value;
                //        P151 = P151 + att.Value;
                //        break;

                //    case "D05":                //AVENA
                //        D05 = att.Value;
                //        P1 = P1 + att.Value;
                //        P15 = P15 + att.Value;
                //        P151 = P151 + att.Value;
                //        break;

                //    case "D06":               //mais
                //        D06 = att.Value;
                //        P1 = P1 + att.Value;
                //        P15 = P15 + att.Value;
                //        P151 = P151 + att.Value;
                //        break;

                //    case "D07":                 //riso
                //        D07 = att.Value;
                //        P1 = P1 + att.Value;    // Semintativi + riso
                //        P15 = P15 + att.Value;    //Cereali + riso
                //        P112 = P112 + att.Value;  // gruppo Riso + riso
                //        break;

                //    case "D08":               //ALTRI CEREALI
                //        D08 = att.Value;
                //        P1 = P1 + att.Value;
                //        P15 = P15 + att.Value;
                //        P151 = P151 + att.Value;
                //        break;

                //    case "D09":                //LEGUMI SECCHI
                //        D09 = att.Value;
                //        P1 = P1 + att.Value;     // seminativi + legumi
                //        break;

                //    case "D10":                 //PATATE
                //        D10 = att.Value;
                //        P1 = P1 + att.Value;
                //        //P17 = P17 + att.Value;    //piante sarchiate + patate
                //        break;

                //    case "D11":               //bietola da zucchero
                //        D11 = att.Value;
                //        P1 = P1 + att.Value;
                //        //P17 = P17 + att.Value;    //piante sarchiate + patate
                //        break;

                //    case "D14A":             //ORTO IN PIENO CAMPO
                //        D14A = att.Value;
                //        P1 = P1 + att.Value;     // seminativi + ortaggi p.c.
                //        break;

                //    case "D14B":                //ORTO INDUSTRIALE
                //        D14B = att.Value;
                //        P2 = P2 + att.Value;    //Ortofloricoltura intensiva + orto industriale
                //        break;

                //    case "D15":                 //ORTO IN SERRA
                //        D15 = att.Value;
                //        P2 = P2 + att.Value;    //Ortofloricoltura intensiva + orto sottoserra
                //        break;

                //    case "D16":               //FIORI IN PIENA ARIA
                //        D16 = att.Value;
                //        P2 = P2 + att.Value;     //Ortofloricoltura intensiva + fiori p.a.
                //        break;

                //    case "D17":                //FIORI IN SERRA
                //        D17 = att.Value;
                //        P2 = P2 + att.Value;     //Ortofloricoltura intensiva + fiori in serra
                //        break;

                //    case "D18B":               //ALTRE FORAGGERE
                //        D18B = att.Value;
                //        //P1 = P1 + att.Value;
                //        //P4 = P4 + att.Value;   // 13/1/2012
                //        break;

                //    case "D19":                //SEMENTI E PIANTINE  (non ci sono utilizzi per questa rubrica in GAIA)
                //        D19 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D20":                //ALTRI SEMINATIVI (in GAIA: Altri seminativi aziendali, Altri terreni SAU, Affitti a terzi sotto l//anno)
                //        D20 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D22":                 //Terreni a riposo
                //        D22 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D23":                   //TABACCO
                //        D23 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D24":                    //LUPPOLO
                //        D24 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D26":                     //Colza e ravizzone
                //        D26 = att.Value;
                //        P1 = P1 + att.Value;
                //        P16 = P16 + att.Value;  //Oleaginose + colza
                //        break;

                //    case "D27":                    //GIRASOLE
                //        D27 = att.Value;
                //        P1 = P1 + att.Value;
                //        P16 = P16 + att.Value;
                //        break;

                //    case "D28":                    //SOIA
                //        D28 = att.Value;
                //        P1 = P1 + att.Value;
                //        P16 = P16 + att.Value;
                //        break;

                //    case "D29":                    //SEMI DI LINO (in gaia il lino e nella rubrica D31)
                //        D28 = att.Value;
                //        P1 = P1 + att.Value;
                //        P16 = P16 + att.Value;
                //        break;

                //    case "D30":                    //ALTRE OLEAGINOSE
                //        D30 = att.Value;
                //        P1 = P1 + att.Value;
                //        P16 = P16 + att.Value;
                //        break;

                //    case "D31":                    //LINO
                //        D31 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D32":                    //CANAPA, cotone e ginestra
                //        D32 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D33":                    //Altre colture tessili
                //        D33 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D34":                   //Piante aromatiche, medicinali
                //        D34 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "D35":                    //Altre colture industriali
                //        D35 = att.Value;
                //        P1 = P1 + att.Value;
                //        break;

                //    case "G01A": //frutta temperata
                //        G01A = att.Value;
                //        P3 = P3 + att.Value;    //Coltivazioni permanenti + frutta temp
                //        P32 = P32 + att.Value;  ////Frutta e agrumi +  frutta temp
                //        break;

                //    case "G01B":       // frutta tropicale
                //        G01B = att.Value;
                //        P3 = P3 + att.Value;
                //        P32 = P32 + att.Value;
                //        break;

                //    case "G01C":       //frutta a guscio
                //        G01C = att.Value;
                //        P3 = P3 + att.Value;
                //        P32 = P32 + att.Value;
                //        break;

                //    case "G02": // agrumi
                //        G02 = att.Value;
                //        P3 = P3 + att.Value;
                //        P32 = P32 + att.Value;
                //        break;

                //    case "G03A":   //olive da tavola
                //        G03A = att.Value;
                //        P3 = P3 + att.Value;
                //        break;

                //    case "G03B":  //olive da olio
                //        G03B = att.Value;
                //        P3 = P3 + att.Value;
                //        break;

                //    case "G04A":   //vite qualità
                //        G04A = att.Value;
                //        P3 = P3 + att.Value;
                //        P31 = P31 + att.Value;
                //        break;

                //    case "G04B":  //vite comune
                //        G04B = att.Value;
                //        P3 = P3 + att.Value;
                //        P31 = P31 + att.Value;
                //        break;

                //    case "G04C":   //vite tavola
                //        G04C = att.Value;
                //        P3 = P3 + att.Value;
                //        P31 = P31 + att.Value;
                //        break;

                //    case "G04D":   //vite UVA PASSA
                //        G04D = att.Value;
                //        P3 = P3 + att.Value;
                //        P31 = P31 + att.Value;
                //        break;

                //    case "G05":    //vivai
                //        G05 = att.Value;
                //        P2 = P2 + att.Value;
                //        break;

                //    case "G06": //Altre colture permanenti
                //        G06 = att.Value;
                //        P3 = P3 + att.Value;
                //        break;

                //    case "G07": //Colture permanenti sotto vetro
                //        G07 = att.Value;
                //        P3 = P3 + att.Value;
                //        break;

                //    case "I02":  //funghi coltivati
                //        I02 = att.Value;
                //        P2 = P2 + att.Value;    //Ortofloricoltura intensiva +  funghi
                //        break;

                //    case "J01": //Equini
                //        J01 = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        break;

                //    case "J03":  //Bovini maschi 1-2 anni
                //        J03 = att.Value;
                //        P4 = P4 + att.Value; // Erbivori e foraggio + b. maschi 1-2
                //        P40 = P40 + att.Value; // Erbivori + maschi 1-2
                //        P46 = P46 + att.Value; //Bovini + b. maschi 1-2
                //        break;

                //    case "J04": ////Bovini femmine 1-2 anni + Vacche da riforma + altre vacche
                //        J04 = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        P45 = P45 + att.Value; // bovini da latte + altre vacche
                //        P46 = P46 + att.Value;
                //        break;

                //    case "J05":  ////Bovini maschi >2 anni
                //        J05 = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        P46 = P46 + att.Value;
                //        break;

                //    case "J06":  ////Giovenche >2 anni
                //        J06 = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        P45 = P45 + att.Value;
                //        P46 = P46 + att.Value;
                //        break;

                //    case "J07": //vacche da latte
                //                //mox _NrVacche = _NrVacche + Rs!dimensione;
                //        J07 = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        P45 = P45 + att.Value;
                //        P46 = P46 + att.Value;
                //        break;

                //    case "J08":     //vacche nutrici
                //                    //mox _NrVacche = _NrVacche + Rs!dimensione;
                //        _NrVacche = att.Value;

                //        J08 = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        P46 = P46 + att.Value;
                //        break;

                //    case "J02":  //Bovini < 1 anno
                //                 //mox _NrVitelli = Rs!dimensione;
                //        _NrVitelli = att.Value;

                //        J02 = att.Value;
                //        if (J02 > 0 && _NrVitelli > 0)
                //        {
                //            J02unit = J02 / _NrVitelli;
                //        }
                //        break;


                //    case "J09A":   //pecore
                //        J09A = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        break;

                //    case "J09B":  //altri ovini
                //        J09B = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        break;

                //    case "J10A":   //capre
                //        J10A = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        break;

                //    case "J10B":  //altri caprini
                //        J10B = att.Value;
                //        P4 = P4 + att.Value;
                //        P40 = P40 + att.Value;
                //        break;

                //    case "D12":               //SARCHIATE DA FORAGGIO
                //        D12 = att.Value;
                //        //P1 = P1 + att.Value;
                //        //if (P40 == 0)
                //        //{         // 12/1/2012
                //        //    P17 = P17 + att.Value;    //piante sarchiate + patate
                //        //}
                //        break;

                //    case "F01":                  //Prati permanenti e pascoli - esclusi i pascoli magri
                //        F01 = att.Value;
                //        //if (P4 > 0)
                //        //{
                //        //    P4 = P4 + att.Value;
                //        //}
                //        //else 
                //        //{
                //        //    P1 = P1 + att.Value;
                //        //}
                //        break;

                //    case "F02":                 // Prati permanenti e pascoli - pascoli magri
                //        F02 = att.Value;
                //        //if (P4 > 0)
                //        //{
                //        //    P4 = P4 + att.Value;
                //        //}
                //        //else 
                //        //{
                //        //    P1 = P1 + att.Value;         //gruppo "erbivori forraggio" + pascoli magri
                //        //}
                //        break;

                //    case "D18A":             //PRATI AVVICENDATI
                //        D18A = att.Value;
                //        //if (P4 > 0)
                //        //{
                //        //    P4 = P4 + att.Value;     // gruppo "erbivori e foraggio"  // 13/1/2012
                //        //}
                //        //else 
                //        //{
                //        //    P1 = P1 + att.Value;     // seminativi + prati
                //        //}
                //        break;

                //    case "J11": //lattonzoli
                //        J11 = NullToZero(att.Value);
                //        P5 = P5 + NullToZero(att.Value);
                //        P51 = P51 + NullToZero(att.Value);
                //        break;

                //    case "J12": //scrofe
                //        J12 = NullToZero(att.Value);
                //        P5 = P5 + NullToZero(att.Value);
                //        P51 = P51 + NullToZero(att.Value);
                //        break;

                //    case "J13": //altri suini
                //        J13 = NullToZero(att.Value);
                //        P5 = P5 + NullToZero(att.Value);
                //        P51 = P51 + NullToZero(att.Value);
                //        break;

                //    case "J14": //polli da carne
                //        J14 = NullToZero(att.Value);
                //        P5 = P5 + NullToZero(att.Value);
                //        P52 = P52 + NullToZero(att.Value);
                //        break;

                //    case "J15":   //galline
                //        J15 = NullToZero(att.Value);
                //        P5 = P5 + NullToZero(att.Value);
                //        P52 = P52 + NullToZero(att.Value);
                //        break;

                //    case "J16D":  //altri volatili
                //        J16D = NullToZero(att.Value);
                //        P5 = P5 + NullToZero(att.Value);
                //        P52 = P52 + NullToZero(att.Value);
                //        break;

                //    case "J17": //coniglie
                //        J17 = NullToZero(att.Value);
                //        P5 = P5 + NullToZero(att.Value);
                //        break;

                //    case "J18":  //api
                //        J18 = att.Value;
                //        break;
                //}
                #endregion

                switch (att.Key.ToUpper())
                {
                    //non c'è chiedere - logicamente se non esiste vuol dire che per i paesi europei non verrà chiesto ma serve una nostra codifica
                    //case "P91":                 //FORAGGERE REIMPIEGATE //NON C'è
                    //    P91 = true;
                    //    break;

                    case "B_1_1_1":                //FRUMENTO TENERO
                        D01 = att.Value;
                        P1 = P1 + att.Value;    //Seminitavi + f.duro
                        P15 = P15 + att.Value;  //Cereali + f.duro
                        P151 = P151 + att.Value;    //Cereali senza riso + f.duro
                        break;

                    case "B_1_1_2":               //FRUMENTO DURO
                        D02 = att.Value;
                        P1 = P1 + att.Value;    //Seminitavi + f.tenero
                        P15 = P15 + att.Value;  //Cereali + f.tenero
                        P151 = P151 + att.Value;    //Cereali senza riso + f.tenero
                        break;

                    case "B_1_1_3":                //SEGALE
                        D03 = att.Value;
                        P1 = P1 + att.Value;
                        P15 = P15 + att.Value;
                        P151 = P151 + att.Value;
                        break;

                    case "B_1_1_4":                //ORZO
                        D04 = att.Value;
                        P1 = P1 + att.Value;
                        P15 = P15 + att.Value;
                        P151 = P151 + att.Value;
                        break;

                    case "B_1_1_5":                //AVENA
                        D05 = att.Value;
                        P1 = P1 + att.Value;
                        P15 = P15 + att.Value;
                        P151 = P151 + att.Value;
                        break;

                    case "B_1_1_6":               //mais
                        D06 = att.Value;
                        P1 = P1 + att.Value;
                        P15 = P15 + att.Value;
                        P151 = P151 + att.Value;
                        break;

                    case "B_1_1_7":                 //riso
                        D07 = att.Value;
                        P1 = P1 + att.Value;    // Semintativi + riso
                        P15 = P15 + att.Value;    //Cereali + riso
                        P112 = P112 + att.Value;  // gruppo Riso + riso
                        break;

                    case "B_1_1_99":               //ALTRI CEREALI
                        D08 = att.Value;
                        P1 = P1 + att.Value;
                        P15 = P15 + att.Value;
                        P151 = P151 + att.Value;
                        break;

                    case "B_1_2":                //LEGUMI SECCHI
                    case "B_1_2_1":              //PISELLI SECCHI - peas, field beans and sweet lupines
                    case "B_1_2_2":              //ALTRI LEGUMI 
                        D09 = att.Value;
                        P1 = P1 + att.Value;     // seminativi + legumi
                        break;

                    case "B_1_3":                 //PATATE
                        D10 = att.Value;
                        P1 = P1 + att.Value;
                        //P17 = P17 + att.Value;    //piante sarchiate + patate
                        break;

                    case "B_1_4":               //bietola da zucchero
                        D11 = att.Value;
                        P1 = P1 + att.Value;
                        //P17 = P17 + att.Value;    //piante sarchiate + patate
                        break;

                    case "B_1_7_1_1":             //ORTO IN PIENO CAMPO
                        D14A = att.Value;
                        P1 = P1 + att.Value;     // seminativi + ortaggi p.c.
                        break;

                    case "B_1_7_1_2":                //ORTO INDUSTRIALE
                        D14B = att.Value;
                        P2 = P2 + att.Value;    //Ortofloricoltura intensiva + orto industriale
                        break;

                    case "B_1_7_2":                 //ORTO IN SERRA
                        D15 = att.Value;
                        P2 = P2 + att.Value;    //Ortofloricoltura intensiva + orto sottoserra
                        break;

                    case "B_1_8_1":               //FIORI IN PIENA ARIA
                        D16 = att.Value;
                        P2 = P2 + att.Value;     //Ortofloricoltura intensiva + fiori p.a.
                        break;

                    case "B_1_8_2":                //FIORI IN SERRA
                        D17 = att.Value;
                        P2 = P2 + att.Value;     //Ortofloricoltura intensiva + fiori in serra
                        break;

                    case "B_1_9_1":             //PRATI AVVICENDATI
                        D18A = att.Value;
                        //if (P4 > 0)
                        //{
                        //    P4 = P4 + att.Value;     // gruppo "erbivori e foraggio"  // 13/1/2012
                        //}
                        //else 
                        //{
                        //    P1 = P1 + att.Value;     // seminativi + prati
                        //}
                        break;

                    case "B_1_9_2":               //ALTRE FORAGGERE
                    case "B_1_9_2_1":            
                    case "B_1_9_2_2":             
                        D18B = att.Value;
                        //P1 = P1 + att.Value;
                        //P4 = P4 + att.Value;   // 13/1/2012
                        break;

                    case "B_1_10":                //SEMENTI E PIANTINE  (non ci sono utilizzi per questa rubrica in GAIA)
                        D19 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_11":                //ALTRI SEMINATIVI (in GAIA: Altri seminativi aziendali, Altri terreni SAU, Affitti a terzi sotto l//anno)
                        D20 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_12":                 //Terreni a riposo
                        D22 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_6_1":                   //TABACCO
                        D23 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_6_2":                    //LUPPOLO
                        D24 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_6_4":                     //Colza e ravizzone
                        D26 = att.Value;
                        P1 = P1 + att.Value;
                        P16 = P16 + att.Value;  //Oleaginose + colza
                        break;

                    case "B_1_6_5":                    //GIRASOLE
                        D27 = att.Value;
                        P1 = P1 + att.Value;
                        P16 = P16 + att.Value;
                        break;

                    case "B_1_6_6":                    //SOIA
                        D28 = att.Value;
                        P1 = P1 + att.Value;
                        P16 = P16 + att.Value;
                        break;

                    case "B_1_6_7":                    //SEMI DI LINO (in gaia il lino e nella rubrica D31)
                        D28 = att.Value;
                        P1 = P1 + att.Value;
                        P16 = P16 + att.Value;
                        break;

                    case "B_1_6_8":                    //ALTRE OLEAGINOSE
                        D30 = att.Value;
                        P1 = P1 + att.Value;
                        P16 = P16 + att.Value;
                        break;

                    case "B_1_6_9":                    //LINO
                        D31 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_6_10":                    //CANAPA, cotone e ginestra
                        D32 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_6_11":                    //Altre colture tessili
                        D33 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_6_12":                   //Piante aromatiche, medicinali
                        D34 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_1_6_99":                    //Altre colture industriali
                        D35 = att.Value;
                        P1 = P1 + att.Value;
                        break;

                    case "B_4_1_1_1": //frutta temperata
                    case "B_4_1_2":  // BACCHE
                        G01A = att.Value;
                        P3 = P3 + att.Value;    //Coltivazioni permanenti + frutta temp
                        P32 = P32 + att.Value;  ////Frutta e agrumi +  frutta temp
                        break;

                    case "B_4_1_1_2":       // frutta tropicale
                        G01B = att.Value;
                        P3 = P3 + att.Value;
                        P32 = P32 + att.Value;
                        break;

                    case "B_4_1_3":       //frutta a guscio
                        G01C = att.Value;
                        P3 = P3 + att.Value;
                        P32 = P32 + att.Value;
                        break;

                    case "B_4_2": // agrumi
                        G02 = att.Value;
                        P3 = P3 + att.Value;
                        P32 = P32 + att.Value;
                        break;

                    case "B_4_3_1":   //olive da tavola
                        G03A = att.Value;
                        P3 = P3 + att.Value;
                        break;

                    case "B_4_3_2":  //olive da olio
                        G03B = att.Value;
                        P3 = P3 + att.Value;
                        break;

                    case "B_4_4_1":   //vite qualità
                        G04A = att.Value;
                        P3 = P3 + att.Value;
                        P31 = P31 + att.Value;
                        break;

                    case "B_4_4_2":  //vite comune
                        G04B = att.Value;
                        P3 = P3 + att.Value;
                        P31 = P31 + att.Value;
                        break;

                    case "B_4_4_3":   //vite tavola
                        G04C = att.Value;
                        P3 = P3 + att.Value;
                        P31 = P31 + att.Value;
                        break;

                    case "B_4_4_4":   //vite UVA PASSA
                        G04D = att.Value;
                        P3 = P3 + att.Value;
                        P31 = P31 + att.Value;
                        break;

                    case "B_4_5":    //vivai
                        G05 = att.Value;
                        P2 = P2 + att.Value;
                        break;

                    case "B_4_6": //Altre colture permanenti
                        G06 = att.Value;
                        P3 = P3 + att.Value;
                        break;

                    case "B_4_7": //Colture permanenti sotto vetro
                        G07 = att.Value;
                        P3 = P3 + att.Value;
                        break;

                    case "B_6_1":  //funghi coltivati
                        I02 = att.Value;
                        P2 = P2 + att.Value;    //Ortofloricoltura intensiva +  funghi
                        break;

                    case "C_1": //Equini
                        J01 = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        break;

                    case "C_2_1":  //Bovini < 1 anno
                                   //mox _NrVitelli = Rs!dimensione;
                        _NrVitelli = att.Value;

                        J02 = att.Value;
                        if (J02 > 0 && _NrVitelli > 0)
                        {
                            J02unit = J02 / _NrVitelli;
                        }
                        break;

                    case "C_2_2":  //Bovini maschi 1-2 anni
                        J03 = att.Value;
                        P4 = P4 + att.Value; // Erbivori e foraggio + b. maschi 1-2
                        P40 = P40 + att.Value; // Erbivori + maschi 1-2
                        P46 = P46 + att.Value; //Bovini + b. maschi 1-2
                        break;

                    case "C_2_3": ////Bovini femmine 1-2 anni + Vacche da riforma + altre vacche
                        J04 = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        P45 = P45 + att.Value; // bovini da latte + altre vacche
                        P46 = P46 + att.Value;
                        break;

                    case "C_2_4":  ////Bovini maschi >2 anni
                        J05 = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        P46 = P46 + att.Value;
                        break;

                    case "C_2_5":  ////Giovenche >2 anni
                        J06 = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        P45 = P45 + att.Value;
                        P46 = P46 + att.Value;
                        break;

                    case "C_2_6": //vacche da latte
                                //mox _NrVacche = _NrVacche + Rs!dimensione;
                        J07 = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        P45 = P45 + att.Value;
                        P46 = P46 + att.Value;
                        break;

                    case "C_2_99":     //vacche nutrici
                                    //mox _NrVacche = _NrVacche + Rs!dimensione;
                        _NrVacche = att.Value;

                        J08 = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        P46 = P46 + att.Value;
                        break;

                    case "C_3_1_1":   //pecore
                        J09A = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        break;

                    case "C_3_1_99":  //altri ovini
                        J09B = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        break;


                    case "C_3_2_1":   //capre
                        J10A = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        break;

                    case "C_3_2_99":  //altri caprini
                        J10B = att.Value;
                        P4 = P4 + att.Value;
                        P40 = P40 + att.Value;
                        break;

                    case "C_4_1": //lattonzoli
                        J11 = NullToZero(att.Value);
                        P5 = P5 + NullToZero(att.Value);
                        P51 = P51 + NullToZero(att.Value);
                        break;

                    case "C_4_2": //scrofe
                        J12 = NullToZero(att.Value);
                        P5 = P5 + NullToZero(att.Value);
                        P51 = P51 + NullToZero(att.Value);
                        break;

                    case "C_4_99": //altri suini
                        J13 = NullToZero(att.Value);
                        P5 = P5 + NullToZero(att.Value);
                        P51 = P51 + NullToZero(att.Value);
                        break;

                    case "C_5_1": //polli da carne
                    case "C_5_3_1": // J16A - TACCHINI
                    case "C_5_3_2": // J16B - ANATRE
                    case "C_5_3_3": // J16B - OCHE
                    case "C_5_3_4": // J16C - STRUZZI
                        J14 = NullToZero(att.Value);
                        P5 = P5 + NullToZero(att.Value);
                        P52 = P52 + NullToZero(att.Value);
                        break;

                    case "C_5_2":   //galline
                        J15 = NullToZero(att.Value);
                        P5 = P5 + NullToZero(att.Value);
                        P52 = P52 + NullToZero(att.Value);
                        break;


                    case "C_5_3_99":  //altri volatili
                        J16D = NullToZero(att.Value);
                        P5 = P5 + NullToZero(att.Value);
                        P52 = P52 + NullToZero(att.Value);
                        break;

                    case "C_6": //coniglie
                        J17 = NullToZero(att.Value);
                        P5 = P5 + NullToZero(att.Value);
                        break;

                    case "C_7":  //api
                        J18 = att.Value;
                        break;

                    case "B_1_5":               //SARCHIATE DA FORAGGIO
                        D12 = att.Value;
                        //P1 = P1 + att.Value;
                        //if (P40 == 0)
                        //{         // 12/1/2012
                        //    P17 = P17 + att.Value;    //piante sarchiate + patate
                        //}
                        break;

                    case "B_3_1":                  //Prati permanenti e pascoli - esclusi i pascoli magri
                        F01 = att.Value;
                        //if (P4 > 0)
                        //{
                        //    P4 = P4 + att.Value;
                        //}
                        //else 
                        //{
                        //    P1 = P1 + att.Value;
                        //}
                        break;

                    case "B_3_2":                 // Prati permanenti e pascoli - pascoli magri
                        F02 = att.Value;
                        //if (P4 > 0)
                        //{
                        //    P4 = P4 + att.Value;
                        //}
                        //else 
                        //{
                        //    P1 = P1 + att.Value;         //gruppo "erbivori forraggio" + pascoli magri
                        //}
                        break;
                    default:
                        //log.Warning($"missing management code '{att.Key}', please contact system administrator!");
                        throw new Exception($"missing management code '{att.Key}', please contact system administrator!");

                }
            }
        }

        private void confermentOTE()
        {
            //Azzeramento suinetti < 20 Kg in presenza di scrofe
            if (J12 > 0 && J11 > 0)
            {
                P5 = P5 - J11;
                P51 = P51 - J11;
                J11 = 0;
            }

            //Azzeramento Altri ovini (agnellini, agnelloni e arieti) in presenza di Pecore
            if (J09A > 0 && J09B > 0)
            {
                P4 = P4 - J09B;
                P40 = P40 - J09B;
                J09B = 0;
            }

            //Azzeramento Altri caprini (capretti, altri caprini e becchi) in presenza di Capre
            if (J10A > 0 && J10B > 0)
            {
                P4 = P4 - J10B;
                P40 = P40 - J10B;
                J10B = 0;
            }

            //Azzeramento dei vitellini < 1 anno in presenza di Vacche da latte e vacche nutrici
            if (_NrVitelli <= _NrVacche)
            {
                //        P4 = P4 - J02
                //P45 = P45 - J02
                //        P46 = P46 - J02
                J02 = 0;
            }
            else
            {
                if (_NrVacche > 0)
                {
                    PSvitelli = (_NrVitelli - _NrVacche) * J02unit;
                    P4 = P4 + PSvitelli;
                    P40 = P40 + PSvitelli;
                    P45 = P45 + PSvitelli;
                    P46 = P46 + PSvitelli;
                    J02 = PSvitelli;
                }
                else if (_NrVacche == 0)
                {
                    PSvitelli = _NrVitelli * J02unit;
                    P4 = P4 + PSvitelli;
                    P40 = P40 + PSvitelli;
                    P46 = P46 + PSvitelli;
                    J02 = PSvitelli;
                }
            }

            if (P4 > 0)
            {
                P4 = P4 + (D18A + D18B + D12 + F01 + F02);
                P17 = P17 + (D10 + D11);
            }
            else
            {
                P1 = P1 + (D18A + D18B + D12 + F01 + F02);
                P17 = P17 + (D10 + D11 + D12);
            }

            PS = P1 + P2 + P3 + P4 + P5 + J18;

            a = PS / 3;
            B = 2 * PS / 3;
            //c = PS / 10
            //D = PS / 4
            //E = 2 * P45 / 3
            //F = 3 * PS / 4

            //Attribuzione dell//OTE - 20 OTTOBRE 2011

            if (PS == 0)
            {
                _CodiceOTE = 9900;
                return;
            }

            if (P1 > B)
            {
                if ((P15 + P16 + D09) > B)
                { // 12/1/2012 - corretto da P151 a P15
                    if ((P151 + P16 + D09) > B)
                    {
                        _CodiceOTE = 1510;                //Specializzate nei cereali (escluso il riso) e piante oleose e proteiche
                        return;
                    }
                    else if (D07 > B)
                    {
                        _CodiceOTE = 1520;               //Risicole specializzate
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 1530;               //Combinazioni di cereali, riso, piante oleose e piante proteiche
                        return;
                    }
                }

                if (
                    //(P15 + P16 + P17 + D09 + D14A) > 0 || 
                    (P15 + P16 + P17 + D09 + D14A) < B || (F01 + F02) > 0)
                {
                    if (P17 > B)
                    {
                        _CodiceOTE = 1610;               //Specializzate nelle piante sarchiate
                        return;

                    }
                    else if ((P15 + P16 + D09) > a && P17 > a)
                    { // 12/1/2012 - da verificare
                        _CodiceOTE = 1620;               //Combinazioni di cereali, oleaginose, proteiche e sarchiate
                        return;
                    }
                    else if (D14A > B)
                    {
                        _CodiceOTE = 1630;               // Specializzate in orti in pieno campo
                        return;
                    }
                    else if (D23 > B)
                    {
                        _CodiceOTE = 1640;               // Tabacco
                        return;
                    }
                    else if (D25 > B)
                    {
                        _CodiceOTE = 1650;               // Cotone
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 1660;                //Con diverse colture di seminativi combinate (nel vecchio era P1 > B)
                        return;
                    }
                }
            }

            if (P2 > B)
            { // 2 nov 2011 - h 7:47

                if ((D15 + D17) > B)
                {
                    if (D15 > B)
                    {
                        _CodiceOTE = 2110;               // Specializzate in orticoltura da serra
                        return;
                    }
                    else if (D17 > B)
                    {
                        _CodiceOTE = 2120;               //Specializzate in floricoltura e piante ornamentali da serra
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 2130;               //Specializzate in ortofloricoltura mista da serra
                        return;
                    }
                }

                if ((D14B + D16) > B)
                {
                    if (D14B > B)
                    {
                        _CodiceOTE = 2210;               //Specializzate in orticoltura di orti industriali all//aperto
                        return;
                    }
                    else if (D16 > B)
                    {
                        _CodiceOTE = 2220;               //Specializzate in floricoltura e piante ornamentali all//aperto
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 2230;               //Specializzate in ortofloricoltura mista all//aperto
                        return;
                    }
                }

                if ((
                    //(D15 + D17) > 0 && 
                    (D15 + D17) <= B) || (
                    //(D14B + D16) > 0 && 
                    (D14B + D16) <= B) || G05 > B || I02 > B)
                {
                    if (I02 > B)
                    {
                        _CodiceOTE = 2310;               //Specializzate nella coltura dei funghi
                        return;
                    }
                    else if (G05 > B)
                    {
                        _CodiceOTE = 2320;               //Specializzate in vivai
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 2330;               //Specializzate in diverse colture ortofloricole
                        return;
                    }
                }
            }

            if (P3 > B)
            {

                if (P31 > B)
                {
                    if (G04A > B)
                    {
                        _CodiceOTE = 3510;               //Vinicole specializzate nella produzione di vini di qualità
                        return;
                    }
                    else if (G04B > B)
                    {
                        _CodiceOTE = 3520;               //Vinicole specializzate nella produzione di vini non di qualità
                        return;
                    }
                    else if (G04C > B)
                    {
                        _CodiceOTE = 3530;               //Specializzate nella produzione di uve da tavola
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 3540;               //Viticole di altro tipo
                        return;
                    }
                }
                else if (P32 > B)
                {
                    if (G01A > B)
                    {
                        _CodiceOTE = 3610;               //Specializzate produzione frutta fresca di origine temperata
                        return;
                    }
                    else if (G02 > B)
                    {
                        _CodiceOTE = 3620;               //Specializzate produzione di agrumi
                        return;
                    }
                    else if (G01C > B)
                    {
                        _CodiceOTE = 3630;               //Specializzate produzione di frutta a guscio
                        return;
                    }
                    else if (G01B > B)
                    {
                        _CodiceOTE = 3640;               //Specializzate produzione di frutta subtropicale
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 3650;               //Specializzate produzione mista di frutta fresca, agrumi, f. subtropicale e f. a guscio
                        return;
                    }
                }
                else if ((G03A + G03B) > B)
                {
                    _CodiceOTE = 3700;                   //Specializzate in olivicoltura
                    return;
                }
                else
                {
                    _CodiceOTE = 3800;               //Con diversa combinazione di colture permanenti
                    return;
                }
            }

            if (P4 > B)
            {
                if ((J07 > (P40 * 0.75)) && (P40 > (P4 * 0.1)))
                {
                    _CodiceOTE = 4500;               //Bovine specializzate nella produzione di latte
                    return;
                }
                else if ((P46 > (P40 * 0.666) && (J07 <= (P40 * 0.1))) && P40 > (P4 * 0.1))
                {
                    _CodiceOTE = 4600;               //Bovine specializzate — orientamento allevamento e ingrasso
                    return;
                }
                else if ((P46 > (P40 * 0.666) && (J07 > (P40 * 0.1))) && P40 > (P4 * 0.1))
                { // vecchia seconda condizione: J07 > (P40 * 0.1))
                    _CodiceOTE = 4700;               //Bovine — latte, allevamento e ingrasso combinati
                    return;
                }
                else if (P46 <= (P40 * 0.666))
                { // (12/1/2012) se tutti i bovini sono < dei 2/3 degli erbivori
                    if (((J09A + J09B) > (P40 * 0.666)) && P40 > (P4 * 0.1))
                    {
                        _CodiceOTE = 4810;               //Ovine specializzate
                        return;
                    }
                    else if ((P46 > (P40 * 0.333) && ((J09A + J09B) > (P40 * 0.333))) && P40 > (P4 * 0.1))
                    {
                        _CodiceOTE = 4820;               //Con ovini e bovini combinati
                        return;
                    }
                    else if (((J10A + J10B) > (P40 * 0.666)) && P40 > (P4 * 0.1))
                    {
                        _CodiceOTE = 4830;               //Caprine specializzate
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 4840;               //Con vari erbivori
                        return;
                    }
                }
            }


            if (P5 > B)
            {
                if (P51 > B)
                {
                    if (J12 > B)
                    {
                        _CodiceOTE = 5110;               //Specializzate in suini da allevamento
                        return;
                    }
                    else if ((J11 + J13) > B)
                    {
                        _CodiceOTE = 5120;               //Specializzate in suini da ingrasso
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 5130;               //Con suini da allevamento e da ingrasso combinati
                        return;
                    }

                }
                else if (P52 > B)
                {
                    if (J15 > B)
                    {
                        _CodiceOTE = 5210;               //Specializzate in galline ovaiole
                        return;
                    }
                    else if ((J14 + J16D) > B)
                    {
                        _CodiceOTE = 5220;               //Specializzate in pollame da carne
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 5230;               //Con galline ovaiole e pollame da carne combinati
                        return;
                    }
                }
                else
                {
                    _CodiceOTE = 5300;               //Con vari granivori combinati
                    return;
                }
            }

            if ((P1 + P2 + P3) > B && P1 <= B && P2 <= B && P3 <= B)
            {
                if (P2 > a && P3 > a)
                {
                    _CodiceOTE = 6110;                //Ortofloricoltura e colture permanenti combinate
                    return;
                }
                else if (P1 > a && P2 > a)
                {
                    _CodiceOTE = 6120;                //Seminativi e ortofloricoltura combinati
                    return;
                }
                else if (P1 > a && P31 > a)
                {
                    _CodiceOTE = 6130;                //Seminativi e vigneti combinati
                    return;
                }
                else if (P1 > a && P3 > a && (P31 <= a))
                {
                    _CodiceOTE = 6140;                //Seminativi e colture permanenti combinati
                    return;
                }
                else if (P1 > a && (P2 <= a) && (P3 <= a))
                {
                    _CodiceOTE = 6150;                //Policoltura ad orientamento seminativi
                    return;
                }
                else
                {
                    _CodiceOTE = 6160;                //Con policoltura
                    return;
                }
            }

            if ((P4 + P5) > B && P4 <= B && P5 <= B)
            {
                if (P4 > 0 && P4 > P5)
                {
                    if (P45 > (P40 * 0.333) && J07 > (P45 * 0.5))
                    {
                        _CodiceOTE = 7310;               //Poliallevamento ad orientamento latte
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 7320;                //Poliallevamento ad orientamento erbivori non da latte
                        return;
                    }
                }

                if (P4 > 0 && P4 <= P5)
                {
                    if (P45 > (P40 * 0.333) && P5 > a && J07 > (P45 * 0.5))
                    {
                        _CodiceOTE = 7410;                //Poliallevamento con granivori e bovini da latte combinati
                        return;
                    }
                    else
                    {
                        _CodiceOTE = 7420;               //Poliallevamento: granivori ed erbivori non da latte
                        return;
                    }
                }
            }

            if (P1 > a && P4 > a)
            {
                if (P45 > (P40 * 0.333) && (J07 > (P45 * 0.5)) && (P45 < P1))
                {
                    _CodiceOTE = 8310;                //Miste seminativi e bovini da latte
                    return;
                }
                else if (P45 > (P40 * 0.333) && (J07 > (P45 * 0.5)) && P45 >= P1)
                {
                    _CodiceOTE = 8320;                //Miste bovini da latte e seminativi
                    return;
                }
                else if (P1 > P4)
                {
                    _CodiceOTE = 8330;                //Miste seminativi ed erbivori non da latte
                    return;
                }
                else
                {
                    _CodiceOTE = 8340;                //Miste erbivori non da latte e seminativi
                    return;
                }
            }
            else
            {
                if (P1 > a && P5 > a)
                {
                    _CodiceOTE = 8410;                //Miste seminativi e granivori
                    return;
                }
                else if (P3 > a && P4 > a)
                {
                    _CodiceOTE = 8420;                //Miste colture permanenti ed erbivori
                    return;
                }
                else if (J18 > B)
                {
                    _CodiceOTE = 8430;                //Apicole
                    return;
                }
                else
                {
                    _CodiceOTE = 8440;                //Diverse combinazioni colture - allevamenti
                    return;
                }
            }

        }

        /// <summary>
        /// Attribuzione della Dimensione Economica
        /// </summary>
        private void confermentUDE()
        {
            string sql = $"SELECT * FROM EconomicFarmSize WHERE @PS between PS_FROM AND PS_TO ";
            var parm = new List<SqlParameter>();
            parm.Add(new SqlParameter { DbType = DbType.Double, ParameterName = "PS", Value = PS });
            DataTable dt = runCommand(sql, parm);
            if (dt.Rows.Count < 1)
            {
                this._CodiceUDEUE = "0";
                this._CodiceUDEINEA = "00";
                this._ClasseDimensioneEconomicaUE = "n/a";
            }
            else
            {
                this._CodiceUDEUE = dt.Rows[0]["Classe"].ToString();
                this._CodiceUDEINEA = this._CodiceUDEUE.IntToRoman();
                this._ClasseDimensioneEconomicaUE = dt.Rows[0]["DESCRIZ"].ToString();
            }


            #region OLE CODE
            //if (PS == 0)
            //{
            //    this._CodiceUDEUE = "0";
            //    this._CodiceUDEINEA = "00";
            //}
            //else if (PS >= 1 && PS <= 1999)
            //{
            //    this._CodiceUDEUE = "1";
            //    this._CodiceUDEINEA = "I";
            //}
            //else if (PS >= 2000 && PS <= 3999)
            //{
            //    this._CodiceUDEUE = "2";
            //    this._CodiceUDEINEA = "I";
            //}
            //else if (PS >= 4000 && PS <= 7999)
            //{
            //    this._CodiceUDEUE = "3";
            //    this._CodiceUDEINEA = "II";
            //}
            //else if (PS >= 8000 && PS <= 14999)
            //{
            //    this._CodiceUDEUE = "4";
            //    this._CodiceUDEINEA = "III";
            //}
            //else if (PS >= 15000 && PS <= 24999)
            //{
            //    this._CodiceUDEUE = "5";
            //    this._CodiceUDEINEA = "III";
            //}
            //else if (PS >= 25000 && PS <= 49999)
            //{
            //    this._CodiceUDEUE = "6";
            //    this._CodiceUDEINEA = "IV";
            //}
            //else if (PS >= 50000 && PS <= 99999)
            //{
            //    this._CodiceUDEUE = "7";
            //    this._CodiceUDEINEA = "V";
            //}
            //else if (PS >= 100000 && PS <= 249999)
            //{
            //    this._CodiceUDEUE = "8";
            //    this._CodiceUDEINEA = "VI";
            //}
            //else if (PS >= 250000 && PS <= 499999)
            //{
            //    this._CodiceUDEUE = "9";
            //    this._CodiceUDEINEA = "VI";
            //}
            //else if (PS >= 500000 && PS <= 749999)
            //{
            //    this._CodiceUDEUE = "10";
            //    this._CodiceUDEINEA = "VII";
            //}
            //else if (PS >= 750000 && PS <= 999999)
            //{
            //    this._CodiceUDEUE = "11";
            //    this._CodiceUDEINEA = "VII";
            //}
            //else if (PS >= 1000000 && PS <= 1499999)
            //{
            //    this._CodiceUDEUE = "12";
            //    this._CodiceUDEINEA = "VIII";
            //}
            //else if (PS >= 1500000 && PS <= 2999999)
            //{
            //    this._CodiceUDEUE = "13";
            //    this._CodiceUDEINEA = "VIII";
            //}
            //else if (PS >= 3000000)
            //{
            //    this._CodiceUDEUE = "14";
            //    this._CodiceUDEINEA = "VIII";
            //} 
            #endregion

        }

    }
}
