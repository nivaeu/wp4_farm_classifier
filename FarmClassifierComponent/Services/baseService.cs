﻿//
// Copyright (c) CREA 2019-2021.
// This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
// All rights reserved
//
// Project and code is made available under the EU-PL v 1.2 license.
//
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FarmClassifierComponent.Services
{
    public class baseService : IDisposable
    {
        protected SqlConnection cn;
        protected SqlCommand cmd;

        public baseService()
        {
            cn = new SqlConnection(SqlHelper.ConnectionString);
            cn.Open();
        }
        protected DataTable runSimpleCommand(string query)
        {
            cmd = new SqlCommand(query, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tb = new DataTable();
            da.Fill(tb);
            return tb;
        }
        protected DataTable runCommand(string query, List<SqlParameter> parms = null)
        {
            cmd = new SqlCommand(query, cn);
            if (parms != null )
                cmd.Parameters.AddRange(parms.ToArray());

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable tb = new DataTable();
            da.Fill(tb);
            return tb;
        }
        protected object runScalarCommand(string query)
        {
            cmd = new SqlCommand(query, cn);
            return cmd.ExecuteScalar();
        }
        protected double NullToZero(object n)
        {
            double v = 0;

            if (n != null)
            {
                v = Convert.ToDouble(n);
            }
            return v;
        }

        public void Dispose()
        {
            cn.Close();
        }
    }
}
