﻿//
// Copyright (c) CREA 2019-2021.
// This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
// All rights reserved
//
// Project and code is made available under the EU-PL v 1.2 license.
//
using System;
using System.Collections.Generic;
using System.Data;

namespace FarmClassifierComponent.Services
{
    public class QueryService : baseService
    {
        public dynamic Activity(string EuroActivityCode, string euroRegionCode)
        {
            string qry = $"SELECT * FROM Activities A INNER JOIN ActivityDataStandardOutput ADet ON A.ActivityID = ADet.ActivityID LEFT JOIN EuroRegions R ON R.EuroRegionCode = ADet.EuroRegionCode ";
            qry += $" where EuroActivityCode = '{EuroActivityCode}' ";

            if (false == String.IsNullOrWhiteSpace(euroRegionCode))
                qry += $" and  ADet.EuroRegionCode = '{euroRegionCode}' ";

            DataTable dt = runSimpleCommand(qry);
            if (dt.Rows.Count<1)
            {
                return new
                {
                    ActivityCode = EuroActivityCode,
                    Message = $"EuroActivityCode not found!"
                };
            }
            int i = 0;
            List<dynamic> resp = new List<dynamic>();
            foreach (DataRow row in dt.Rows)
            {
                resp.Add(new
                {
                    row = ++i,
                    ActivityID = row[0].ToString(),
                    EuroActivityCode = row["EuroActivityCode"].ToString(),
                    activity = row["DefaultLabel"].ToString(),
                    EuroRegionCode = row["EuroRegionCode"].ToString(),
                    Region = row["Region"].ToString(), 
                    SOValue = row["Value"], 
                    SOYear = row["RefYear"],
                    Unit = row["Unit"].ToString(),
                });

            }
            return resp;
        }

        public dynamic Activities()
        {
            //string qry = $"SELECT EuCode, Un_Mis, A.Label FROM ActivityDetails AD LEFT JOIN Activities A ON AD.ActivityID = A.Attivita_PS_ID where A.Language = 'en' order by 1";
            string qry = $"SELECT * FROM Activities order by EuroActivityCode";

            List<dynamic> resp = new List<dynamic>();
            DataTable dt = runSimpleCommand(qry);
            int i = 0;
            foreach (DataRow row in dt.Rows)
            {
                i++;
                resp.Add(new {
                    RowNumber = i,
                    ActivityID = row[0].ToString(),
                    EuroActivityCode = row["EuroActivityCode"].ToString(),
                    activity = row["DefaultLabel"].ToString(),
                    Unit = row["Unit"].ToString(),
                    //EuOLDCode = row["EuCodeOLD"].ToString(),
                });
            }
            return resp;
        }

        public dynamic EuroRegions()
        {
            string qry = $"SELECT EuroRegionCode, Region FROM EuroRegions";

            List<dynamic> resp = new List<dynamic>();
            DataTable dt = runSimpleCommand(qry);
            int i = 0;
            foreach (DataRow row in dt.Rows)
            {
                i++;
                resp.Add(new
                {
                    Row = i,
                    EuroRegionCode = row[0].ToString(), 
                    Region = row[1].ToString()
                });
            }
            return resp;
        }
    }
}
