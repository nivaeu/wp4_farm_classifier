﻿//
// Copyright (c) CREA 2019-2021.
// This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
// All rights reserved
//
// Project and code is made available under the EU-PL v 1.2 license.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmClassifierComponent.Services
{
    public class SOSeriesService
    {
        public SOSeriesService()
        {
            //range classification year for SO series (CTS)
            //Item1=Range Year from 
            //Item2=Range Year to
            //Item3=Serier Year refer
            rangeClassificationPS = new List<Tuple<int, int, int>>();
            rangeClassificationPS.Add(new Tuple<int, int, int>(int.MinValue, 2009, 2007));
            rangeClassificationPS.Add(new Tuple<int, int, int>(2010, 2015, 2010)); 
            rangeClassificationPS.Add(new Tuple<int, int, int>(2016, int.MaxValue, 2013));
            //problaby for next SO Serie apply this changement
            //rangeClassificationPS.Add(new Tuple<int, int, int>(2016, 2021, 2013));
            //rangeClassificationPS.Add(new Tuple<int, int, int>(2022, int.MaxValue, 2016));
        }
        private List<Tuple<int, int, int>> rangeClassificationPS;

        /// <summary>
        /// Returns the reference year of the series
        /// </summary>
        /// <param name="year">Year classification</param>
        /// <returns></returns>
        public int GetYearSOSeries(int year)
        {
            return rangeClassificationPS.FirstOrDefault(x => year >= x.Item1 && year <= x.Item2).Item3;
        }
    }
}
