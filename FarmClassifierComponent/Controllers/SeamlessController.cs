﻿//
// Copyright (c) CREA 2019-2021.
// This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
// All rights reserved
//
// Project and code is made available under the EU-PL v 1.2 license.
//
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using FarmClassifierComponent.Models;
using FarmClassifierComponent.Services;

namespace FarmClassifierComponent.Controllers
{
    [Route("api/NivaEU")]
    [ApiController]
    public class SeamlessController : ControllerBase
    {
        /// <summary>
        /// List of Euro Regions 
        /// </summary>
        /// <returns></returns>
        [Route("EuroRegions")]
        [HttpGet]
        public ActionResult<dynamic> EuroRegions()
        {
            try
            {
                using (var queryService = new QueryService())
                {
                    return queryService.EuroRegions();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }
        /// <summary>
        /// List of Activities
        /// </summary>
        /// <returns></returns>
        [Route("Activities")]
        [HttpGet]
        public ActionResult<dynamic> Activities()
        {
            try
            {
                using (var queryService = new QueryService())
                {
                    return queryService.Activities();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }
        /// <summary>
        /// Find a specific activity
        /// </summary>
        /// <param name="euroActivityCode">Eu Code of activity</param>
        /// <param name="euroregioncode">UE Region Code (facultative)</param>
        /// <returns></returns>
        [Route("Activities/{euroActivityCode}/{euroregioncode?}")]
        [HttpGet]
        public ActionResult<dynamic> Activity(string euroActivityCode, string euroregioncode="")
        {
            try
            {
                using (var queryService = new QueryService())
                {
                    return queryService.Activity(euroActivityCode, euroregioncode);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }
        /// <summary>
        /// Farm Classifier  Returns classification of a farm based on Activities passed in the parms
        /// </summary>
        ///<remarks>
        /// Sample request:
        ///
        ///     POST /Api/NivaUE/Classifier
        ///{
        ///    "EuroRegionCode" : "ITC2", 
        ///    "FarmActivities": [
        ///        {
        ///            "ActivityCode": "B_1_1_1",
        ///            "Dimension": 1.56
        ///        }
        ///        ,{
        ///            "ActivityCode": "B_1_1_3",
        ///            "Dimension": 2.2
        ///        }
        ///     ]
        ///}
        ///</remarks>
        /// <response code="200">Returns farm classifier</response>
        /// <response code="500">Error occurred</response>  
        ///<param name="model">Payload of request</param>
        ///<returns>Farm Classifier result</returns>
        [Route("Classifier")]
        [HttpPost]
        public ActionResult<GenericResponseResult> Classifier(ClassificationRequest model)
        {
            try
            {
                using (var service = new FarmClassifierService())
                {
                    //service.SetCodeActivityType(model.CodeActivityType);
                    service.Run(model.EuroRegionCode, model.ToActivityDictionary(), model.AccountingYear);
                    //return Ok(service.ClassificazioneAziendale);
                    GenericResponseResult result = new GenericResponseResult { ClassificationResult = service.ClassificazioneAziendale, WarningMessages = service.GetWarningMessages() };
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.ToString());
            }
        }
    }
}