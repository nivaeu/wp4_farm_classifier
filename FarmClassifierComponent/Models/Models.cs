﻿//
// Copyright (c) CREA 2019-2021.
// This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
// All rights reserved
//
// Project and code is made available under the EU-PL v 1.2 license.
//
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FarmClassifierComponent.Models
{
    /// <summary>
    /// Farm Classification Response
    /// </summary>
    public class ClassificationResult
    {
        #region public properties
        public int ReferenceSOYear { get; set; }
        //public string CodeActivityTypeUsed { get; set; }

        public double StandardOutput
        {
            get;
            set;
        }

        public Int32 TypeFarmCodeOTE
        {
            get;
            set;
        }

        //public string DescrizioneOTE
        //{
        //    get;
        //    set;
        //}

        public string PrincipalTypeOfFarming
        {
            get;
            set;
        }

        public string SubdivisionParticularTypeFarming
        {
            get;
            set;
        }

        //public string CodClasseDE
        //{
        //    get;
        //    set;
        //}

        //public string ClasseDimensioneEconomica
        //{
        //    get;
        //    set;
        //}
        //public string CodClasseDEINEA
        //{
        //    get;
        //    set;
        //}

        //public string ClasseDimensioneEconomicaINEA
        //{
        //    get;
        //    set;
        //}

        public string FarmEconomicSizeCode
        {
            get;
            set;
        }

        public string FarmEconomicSizeClass
        {
            get;
            set;
        }

        #endregion 
    }
    /// <summary>
    /// Classification Request model
    /// </summary>
    public class ClassificationRequest
    {
        /// <summary>
        /// Region Code EU
        /// </summary>
        [Required]
        public string EuroRegionCode { get; set; }
        /// <summary>
        /// Accounting Year of farm (PS) - not mandatory, if it not passed is considered the last reference year of the SO in the system
        /// </summary>
        public int? AccountingYear { get; set; }
        /// <summary>
        /// List of FarmActivities
        /// </summary>
        public List<FarmActivity> FarmActivities { get; set; } = new List<FarmActivity>();
        /// <summary>
        /// Code Activity Type EUCODE default (B_1_1_1) or OLDEUCODE (J09B)
        /// </summary>
        public string CodeActivityType { get; set; } = "EUCODE";
    }
    /// <summary>
    /// Company Activity Item
    /// </summary>
    public class FarmActivity
    {
        /// <summary>
        /// Code Activity
        /// </summary>
        public string ActivityCode { get; set; }
        /// <summary>
        /// Dimension Ha/aa
        /// </summary>
        public double Dimension { get; set; }
    }

    public class GenericResponseResult
    {
        public ClassificationResult ClassificationResult { get; set; } = new ClassificationResult();
        public List<string> WarningMessages = new List<string>();
    }
}