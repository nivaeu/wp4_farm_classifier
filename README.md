**<h2>NivaFarmClassifierAPIComponent</h2>**


**NivaFarmClassifierAPIComponent** is a RestFul API for the classification of a farm. 


# Introduction
This sub-project is part of the ["New IACS Vision in Action”
--- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for
e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate
data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under
grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on
https://gitlab.com/niva



This project's aim is to share farm classifier API system based on rules of EU DG-AGRI (Reg. (EC) No 1242/2008)


**Getting Started**

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 


**Prerequisites**

Necessary software to use this repository


Visual Studio 2017 or above: https://visualstudio.microsoft.com/downloads/

.NET Core 2.1: https://dotnet.microsoft.com/download/dotnet-core/2.1

RDBMS: MSSQL SERVER EXPRESS EDITION - release 12 or above : https://www.microsoft.com/it-it/download/details.aspx?id=55994


**Installing**

RDBMS:

- Install MS SQL SERVER 2012 EXPRESS EDITION or above release with SMSS tool (Sql Server Management System)
- After installation open within SMSS the file "Create_Initial.sql" in the /DBScript Folder and execute it

IDE:

- Install Visual Studio 2017 Free Community Edition 


Clone the repository

Windows example:

In console or with powershell write this:


git clone https://gitlab.com/foldename/NivaFarmClassifierAPIComponent.git



Running

- Open file NivaFarmClassifier.sln with Visual Studio 

- Check the database connection string in the appsettings.json file and reaplce with correct name of your database instance

- Press Run in Visual Studio and see port local started

- Use Swagger to try classification or client software for call REST API POST for example POSTMAN 

- if you use a client to call REST API, create and fill properly the payload base on ClassificationRequest.cs model



**Swagger**

![plot](./Images/swagger_1.png)


![plot](./Images/swagger_2.png)


![plot](./Images/swagger_3.png)






